#ifndef __wjson_LValue__
#define __wjson_LValue__

#include "Value.hpp"

namespace wjson {
  /**
   * @class LValue
   * @description Implements a minimal C/C++ ontology view of a [JSON](https://www.json.org) data structure.
   * @extends Value
   * - Such a LValue has the capability to interface with a ontology reasoner and other tools.
   * - Value functions, beyond [Value](./Value.html) functions
   *   - Input/Output:
   *     - [`setTurtoise(String value)`](#setTurtoise) allows to append a data structure serialized as a [Turtoise](./turtoise.pdf) set of triplets, using [N-Triples](https://en.wikipedia.org/wiki/N-Triples) syntax.
   *    - [`getTurtoise()`](#getTurtoise) allows to output the data structure as a serialized [Turtoise](./turtoise.pdf) set of triplets, using [N-Triples](https://en.wikipedia.org/wiki/N-Triples) syntax.
   *   - Interface with the external reasoner:
   *     - [`deduce()`](#deduce) runs an external reasoner to obtain deduced features.
   * @slides ./turtoise.pdf
   * @param {string|bool|int|uint|double|float|JSON} [value] The initial value.
   * @param {bool} [parse=false] For a string value, if true parses the string using the [Turtoise](./turtoise.pdf) semantic.
   * @param {bool} [sort_names=false] For a parsed string value, if true sorts the names in alphabetic order, as performed by Value::sortNames().
   */
  class LValue: public Value {
public:
    LValue();
    LValue(const Value& value);
    LValue(const LValue& value);
    LValue(String value, bool parse, bool sort_names = false);
    template < typename T > LValue(T value) : Value(value) {}

    /**
     * @function getTurtoise
     * @memberof LValue
     * @instance
     * @description Outputs the data structure as a serialized [Turtoise](./turtoise.pdf) set of triplets.
     * - The standard OWL prefixes are predefined:
     * ```
     * &#64;prefix owl: <http://www.w3.org/2002/07/owl#> .
     * &#64;prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
     * &#64;prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
     * &#64;prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
     * &#64;prefix xml: <http://www.w3.org/XML/1998/namespace> .
     * ```
     * @return The data structure content using [N-Triples](https://en.wikipedia.org/wiki/N-Triples) syntax.
     */
    std::string getTurtoise() const;

    /**
     * @function setTurtoise
     * @memberof LValue
     * @instance
     * @description Appends a data structure serialized as a [Turtoise](./turtoise.pdf) set of triplets.
     * @param {String} [string] The input triples using the [N-Triples](https://en.wikipedia.org/wiki/N-Triples) syntax.
     * @return {Value} This value, allowing to chain set methods.
     */
    LValue& setTurtoise(String string);
private:
    static Value turtoisePrefixes;
public:

    /**
     * @function deduce
     * @memberof LValue
     * @instance
     * @description Runs an external reasoner to obtain deduced features.
     * - It is not available in the cygwin environment.
     * - The present implementation interfaces with Protege using the [`run_protege.sh`](./global.html#run_protege.html) with
     *   - `/tmp/run_protege.input.n3` as input,
     *   - `/tmp/run_protege.input.n3.inferred.owl.n3` as output,
     *   - `/tmp/run_protege.log` with the complete process log.
     * @param {Value} [value] A `Value*` pointer output value, by default append deduced features to this value.
     * @param {string} [wjson="./src"] The wjson current source files directory to access to the required scripts.
     */
    void deduce(LValue *value = NULL, String wjson = "./src", bool test = false);

    /**
     * @function readTriples
     * @memberof LValue
     * @static
     * @description Reads a string and return the best parsed structure.
     *  - Implements a [N-Triples](https://en.wikipedia.org/wiki/N-Triples) weak parser.
     *    - The triple IRI of the form `<IRI>` are unquoted (suppression of the `<` and `>`) and interal `>` is unescaped.
     *    - The triple string of the form `"string"` are unquoted (suppression of the `"`) and internal `"` is unescaped.
     *    - The IRI of the form `local:` are unescaped (suppression of the `local:` prefix).
     * @param {string} string The input string to parse.
     * @return {Value} A `[[subject predicate object] …]` array with the triplets.
     */
    static Value readTriples(String string);
  };
}

#endif
