#include "LValue.hpp"
#include "regex.hpp"
#include "file.hpp"
#include <unistd.h>

namespace wjson {
  Value turtoisePrefixes("{"
                         "owl: http://www.w3.org/2002/07/owl#"
                         "rdfs: http://www.w3.org/2000/01/rdf-schema#"
                         "rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                         "xsd: http://www.w3.org/2001/XMLSchema#"
                         "xml: http://www.w3.org/XML/1998/namespace"
                         "}", true);

  /** Implements a N-Triples writer.
   * @class TurtoiseWriter
   */
  class TurtoiseWriter {
public:
    std::string buffer;

    /**
     * @function write
     * @memberof TurtoiseWriter
     * @description Append the data structure serialization to the current buffer.
     * @param {JSON} value The input JSON value to serialize.
     * - If the value is atomic no triplet are generated.
     * @return The set of triplets in N-Triples syntax.
     */
    String write(JSON value)
    {
      buffer = "";
      // Add standard prefixes
      write_value(value, "", turtoisePrefixes);
      return buffer;
    }
private:
    // Serializes a value at a certain subject location in the hierarchy given inherited prefixes and base
    void write_value(JSON value, String subject = "", Value prefixes = Value::EMPTY, std::string base = "local:")
    {
      if(value.isRecord()) {
        // Registers base and prefixes if any
        if(value.isMember("@base")) {
          base = aidesys::regexReplace(value.get("@base", ""), "([^/])$", "$1/");
        }
        if(value.isMember("@prefix")) {
          for(auto it = value.at("@prefix").getNames().begin(); it != value.at("@prefix").getNames().end(); ++it) {
            prefixes[*it] = aidesys::regexReplace(value.at("@prefix").get(*it, ""), "([^/])$", "$1/");
          }
        }
        // Adds the main tr
        if(buffer == "") {
          buffer += "<" + base + "> <" + prefixes.at("rdf").asString() + "type> <" + prefixes.at("owl").asString() + "Ontology> .\n";
        }
        // Serializes the triplets
        for(auto it = value.getNames().begin(); it != value.getNames().end(); ++it) {
          String name = *it;
          String predicate = getSerializedValue(name, prefixes, base, true);
          // Adds the corresponding triple
          if(value.at(name).isRecord()) {
            // Serializes the record items
            String object = (subject == "" ? "" : subject + "/") + aidesys::regexReplace(name, "/", "%2F");
            write_value(value.at(name), object, prefixes, base);
          } else {
            buffer += "<" + base + subject + "> " + predicate + " " + getSerializedValue(value.at(name).asString(), prefixes, base, false) + " .\n";
          }
        }
      }
    }
    static std::string getSerializedValue(std::string value, JSON prefixes, std::string base, bool predicate)
    {
      // Expands prefixes
      if(aidesys::regexMatch(value, "([a-z]*):.*")) {
        std::string prefix = aidesys::regexReplace(value, "([a-z]*):.*", "$1");
        if(prefixes.isMember(prefix)) {
          value = prefixes.get(prefix, "") + value.substr(prefix.length() + 1);
        }
      }
      // Quotes IRIs, predicates and values as string, escaping final quote in content
      if(aidesys::regexMatch(value, "([a-z]+:).*")) {
        value = "<" + aidesys::regexReplace(value, ">", "%3E") + ">";
      } else if(predicate) {
        value = "<" + base + value + ">";
      } else {
        value = "\"" + aidesys::regexReplace(value, "\"", "%22") + "\"";
      }
      return value;
    }
  };

  /** Implements a N-Triples weak parser.
   * - The triple IRI of the form `<IRI>` are unquoted (suppression of the `<` and `>`) and internal `>` is unescaped.
   * - The triple string of the form `"string"` are unquoted (suppression of the `"`) and internal `"` is unescaped.
   * - The IRI of the form `local:` are unescaped (suppression of the `local:` prefix).
   * @class N_TriplesReader
   */
  class N_TriplesReader {
    const char *chars;
    unsigned int index, length;
public:

    /**
     * @function read
     * @memberof N_TriplesReader
     * @description Reads a string and return the best parsed structure.
     * @param {string} string The input string to parse.
     * @param {value} triplets A `[[subject predicate object] …]` array with the triplets.
     */
    void read(String string, Value& triples)
    {
      // Works on a char buffer
      chars = string.c_str(), index = 0, length = string.length();
      // Scans "subject predicate object ." triplets
      for(bool found = true; found;) {
        std::string subject, predicate, object;
        read_word(subject);
        read_word(predicate);
        read_word(object);
        found = next_dot();
        if(found) {
          unsigned int l = triples.length();
          triples[l][0] = subject, triples[l][1] = predicate, triples[l][2] = object;
        }
      }
    }
private:
    // Reads a word
    void read_word(std::string& word)
    {
      next_space();
      if(index < length) {
        // Unquotes IRI and strings and unescapes chars
        switch(chars[index]) {
        case '<':
          read_quoted_word('>', word);
          word = aidesys::regexReplace(aidesys::regexReplace(word, "%3E", ">"), "%2F", "/");
          word = aidesys::regexReplace(word, "^local:", "");
          break;
        case '"':
          read_quoted_word('"', word);
          word = aidesys::regexReplace(word, "%22", "\"");
          break;
        default:
          read_nospace_word(word);
          break;
        }
      }
    }
    // Reads a word between quote
    void read_quoted_word(char quote, std::string& word)
    {
      for(index++; index < length && chars[index] != quote; index++) {
        if(chars[index] == '\\' && index < length - 1 && chars[index + 1] == quote) {
          index++;
          word += chars[index];
        } else {
          word += chars[index];
        }
      }
      if(index < length) {
        index++;
      }
    }
    // Reads a word until next space
    void read_nospace_word(std::string& word)
    {
      for(; index < length && !isspace(chars[index]); index++) {
        word += chars[index];
      }
    }
    // Escapes until next dot, if any
    bool next_dot()
    {
      next_space();
      bool found = index < length && chars[index] == '.';
      if(found) {
        index++;
      }
      return found;
    }
    // Escapes until next non space char
    void next_space()
    {
      // Escapes spaces
      for(; index < length && isspace(chars[index]); index++) {}
      // Escapes comments
      if((index < length) &&
         ((chars[index] == '#') ||
          ((chars[index] == '/') && ((index == length - 1) || (chars[index + 1] == '/')))))
      {
        for(; index < length && chars[index] != '\n'; index++) {}
        next_space();
      }
    }
  };

  /** Implements a Turtoise reader.
   * @class TurtoiseReader
   */
  class TurtoiseReader {
public:

    /**
     * @function read
     * @memberof TurtoiseReader
     * @description Reads a string and return the best parsed structure.
     * @param {string} string The input string to parse.
     * @param {Value} value The output value subject parsed value are inserted.
     */
    void read(String string, Value& value)
    {
      // Parses the triple
      Value triples;
      N_TriplesReader reader;
      reader.read(string, triples);
      decode_base(triples);
      for(unsigned int i = 0; i < triples.length(); i++) {
        add_triple(value, triples.at(i));
      }
      encode_prefix(value, turtoisePrefixes);
    }
private:
    // Reduces base declaration
    void decode_base(Value& triples)
    {
      // Retrieves the 1st base definition if any
      std::string base;
      for(unsigned int i = 0; i < triples.length(); i++) {
        if(triples.at(i).get(1, "").find("@base") == triples.at(i).get(1, "").length() - 5) {
          if(base == "") {
            base = triples.at(i).get(1, "").substr(0, triples.at(i).get(1, "").length() - 5);
            aidesys::alert(triples.at(i).get(0, "") != base || base.find(triples.at(i).get(2, "")) != 0 || base.length() - triples.at(i).get(2, "").length() > 1, " illegal-argument", "in wjson::LValue::setTurtoise spurious triple with @base definition: '" + triples.at(i).asString() + "'");
          } else {
            aidesys::alert(true, " illegal-argument", "in wjson::LValue::setTurtoise spurious triple with a second @base definition: '" + triples.at(i).asString() + "'");
          }
        }
      }
      // Reduces the base in all fields
      if(base != "") {
        for(unsigned int i = 0; i < triples.length(); i++) {
          for(unsigned j = 0; j < 3; j++) {
            if(triples.at(i).get(j, "").find(base) == 0) {
              triples[i][j] = triples.at(i).get(j, "").substr(base.length());
            }
          }
        }
      }
    }
    // Adds a triple to the data structure
    void add_triple(Value& value, Value triple)
    {
      std::string name = triple.get(0, "");
      std::size_t i = name.find('/');
      if(i == std::string::npos) {
        // Adds the related element
        (name == "" ? value : value[name])[triple.get(1, "")] = triple.get(2, "");
      } else {
        // Adds triple for a substructure
        triple[0] = name.substr(i + 1);
        add_triple(value[name.substr(0, i)], triple);
      }
    }
    void encode_prefix(Value& value, Value prefixes = Value::EMPTY, std::string base = "")
    {
      auto names = value.getNames();
      for(auto it = names.begin(); it != names.end(); ++it) {
        String name = *it;
        // Registers a prefix
        if(name == "@prefix") {
          for(auto is = value.at("@prefix").getNames().begin(); is != value.at("@prefix").getNames().end(); ++is) {
            std::string suffix = value.at("@prefix").get(*is, "");
            // Adds a '/' at the end if not yet done
            if(suffix[suffix.length() - 1] != '/') {
              suffix = suffix + '/';
            }
            prefixes[*is] = suffix;
          }
        }
        // Encodes the prefix for values
        if(value.at(name).isRecord()) {
          encode_prefix(value[name], prefixes, base);
        } else {
          std::string new_value;
          if(getEncodePrefix(value.get(name, ""), new_value, prefixes, base)) {
            value[name] = new_value;
          }
        }
        // Encodes the prefix for names
        std::string new_name;
        if(getEncodePrefix(name, new_name, prefixes, base)) {
          value.rename(name, new_name);
        }
      }
    }
    bool getEncodePrefix(String value, std::string& new_value, JSON prefixes, String base)
    {
      if(value.rfind(base) == 0) {
        new_value = value.substr(base.length());
        return true;
      }
      for(auto it = prefixes.getNames().begin(); it != prefixes.getNames().end(); ++it) {
        std::string lhs = *it, rhs = prefixes.get(*it, "");
        if(value.rfind(rhs) == 0) {
          new_value = lhs + ":" + value.substr(rhs.length());
          return true;
        }
      }
      return false;
    }
  };
  LValue::LValue() : Value()
  {}
  LValue::LValue(const Value& value) : Value(value)
  {}
  LValue::LValue(const LValue& value) : Value((const Value&) value)
  {}
  LValue::LValue(String value, bool parse, bool sort_names) : Value(value, parse, sort_names)
  {}

  LValue& LValue::setTurtoise(String value)
  {
    static TurtoiseReader reader;
    reader.read(value, *this);
    return *this;
  }
  std::string LValue::getTurtoise() const
  {
    static TurtoiseWriter writer;
    return writer.write(*this);
  }
  void LValue::deduce(LValue *value, String wjson, bool test)
  {
#ifndef ON_WIN
    if(value == NULL) {
      value = this;
    }
    // Saves this data structure
    std::string fname = "/tmp/run_protege.input.n3";
    aidesys::save(fname, LValue::getTurtoise());
    // Runs Protege and converts to n-triples the result
    if(test) {
      // This ``secret´´ test option simply allows to test the interfacing, rapper is provided by the raptor2 package
      std::string result = aidesys::system("rapper -i ntriples -o ntriples /tmp/run_protege.input.n3 2> /tmp/run_protege.log");
      aidesys::save("/tmp/run_protege.input.n3.inferred.owl.n3", result);
    } else {
      std::string log;
      log += aidesys::system(wjson + "/run_protege.sh " + fname);
      log += aidesys::system(wjson + "/ttl2n3.js < " + fname + ".inferred.owl > " + fname + ".inferred.owl.n3");
      aidesys::save("/tmp/run_protege.running.log", log);
    }
    // Loads the output
    static TurtoiseReader reader;
    reader.read(aidesys::load(fname + ".inferred.owl.n3"), *value);
#else
    aidesys::alert(true, "illegal-state", "wjson::LValue::deduce is not available in the cygwin environment");
#endif
  }
  Value LValue::readTriples(String string)
  {
    N_TriplesReader reader;
    Value value;
    reader.read(string, value);
    return value;
  }
}
