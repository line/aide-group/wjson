#include "FValue.hpp"

namespace wjson {
  FValue::FValue() : Value()
  {}
  FValue::FValue(const Value& value) : Value(value)
  {}
  FValue::FValue(const FValue& value) : Value((const Value&) value)
  {
    if(value.getters != NULL) {
      for(auto it = value.getters->begin(); it != value.getters->end(); ++it) {
        set(it->first, it->second);
      }
    }
  }
  FValue::FValue(String value, bool parse, bool sort_names) : Value(value, parse, sort_names)
  {}
  FValue::~FValue()
  {
    if(getters != NULL) {
      delete getters;
      delete caches;
    }
  }
  const Value& FValue::at(String name) const
  {
    if(getters != NULL) {
      auto it = getters->find(name);
      if(it != getters->end()) {
        if(caches->at(name)) {
          const_cast < FValue & > (*this).Value::set(name, (getters->at(name))(*this));
        } else {
          static Value result;
          result = (getters->at(name))(*this);
          return result;
        }
      }
    }
    return Value::at(name);
  }
  Value& FValue::set(String name, Value (*getter)(const Value&), bool cache)
  {
    if(getters == NULL) {
      getters = new std::unordered_map < std::string, Value (*)(const Value&) > ();
      caches = new std::unordered_map < std::string, bool > ();
    }
    (*getters)[name] = getter;
    (*caches)[name] = cache;
    return *this;
  }
}
