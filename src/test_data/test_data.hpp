#define EVALUATE

#include "file.hpp"
#include "time.hpp"
#include <nlohmann/json.hpp>
#include <json/json.h>
#include "WJSONWriter.hpp"

void test_data_of(String what)
{
  // From https://github.com/miloyip/nativejson-benchmark
  std::string input = aidesys::load("./src/test_data/" + what);
  wjson::Value result;
  printf(("  {\n    input: "+ what + "\n").c_str());
  // test_lohmann
  {
    aidesys::now(false);
    nlohmann::ordered_json value;
    value = nlohmann::json::parse(input);
    result[what]["lohmann"]["read"] = (int) rint(aidesys::now(false, true));
    std::string output = value.dump();
    result[what]["lohmann"]["write"] = (int) rint(aidesys::now(false, true));
  }
  // test_jsoncpp
  {
    aidesys::now(false);
    Json::Value value;
    {
      Json::CharReaderBuilder builder;
      Json::CharReader *reader = builder.newCharReader();
      std::string errors;
      reader->parse(input.c_str(), input.c_str() + input.size(), &value, &errors);
    }
    result[what]["jsoncpp"]["read"] = (int) rint(aidesys::now(false, true));
    {
      Json::StreamWriterBuilder builder;
      std::string output = Json::writeString(builder, value);
    }
    result[what]["jsoncpp"]["write"] = (int) rint(aidesys::now(false, true));
  }
  // test_lohmann
  {
    aidesys::now(false);
    nlohmann::ordered_json value;
    value = nlohmann::ordered_json::parse(input);
    result[what]["ordered_lohmann"]["read"] = (int) rint(aidesys::now(false, true));
    std::string output = value.dump();
    result[what]["ordered_lohmann"]["write"] = (int) rint(aidesys::now(false, true));
  }
  // test_wjson
  {
    wjson::WJSONWriter::force_fast = true;
    aidesys::now(false);
    wjson::Value value(input, true);
    result[what]["wjson"]["read"] = (int) rint(aidesys::now(false, true));
    printf(("    "+result.asString() + "\n").c_str());
    value.check();
    aidesys::now(false);
    std::string output = value.asString();
    result[what]["wjson"]["write"] = (int) rint(aidesys::now(false, true));
    {
      wjson::Value value(output, true);
      aidesys::alert(value.asString() != output, "  illegal-state", "in test_data wjson spurious read/write/read");
    }
  }
  printf(("    "+result.asString() + "\n").c_str());
  printf("  }\n");
}
void test_data()
{
  printf("{\n");
  test_data_of("twitter.json");
  test_data_of("canada.json");
  test_data_of("citm_catalog.json");
  printf("}\n");
}
