#ifndef __wjson_FValue__
#define __wjson_FValue__

#include "Value.hpp"

namespace wjson {
  /**
   * @class FValue
   * @description Implements a minimal C/C++ [JSON](https://www.json.org) dynamic data structure.
   * @extends Value
   * - Such a FValue has the capability to define computed attributes.
   * - Value functions, beyond [Value](./Value.html) functions
   *   - Element access:
   *     - [`value.at(name)`](./Value.html#at) allows to access a static or computed value.
   *   - Modifiers:
   *     - [`value.set(name, Value& (*getter)(const Value&))`](#set) allows to define a value calculation via a function.
   * @param {string|bool|int|uint|double|float|JSON} [value] The initial value.
   * @param {bool} [parse=false] For a string value, if true parses the string, else only copy it.
   * @param {bool} [sort_names=false] For a parsed string value, if true sorts the names in alphabetic order, as performed by Value::sortNames().
   */
  class FValue: public Value {
    std::unordered_map < std::string, Value (*)(const Value&) > *getters = NULL;
    std::unordered_map < std::string, bool > *caches = NULL;
public:
    FValue();
    FValue(const Value& value);
    FValue(const FValue& value);
    FValue(String value, bool parse, bool sort_names = false);
    template < typename T > FValue(T value) : Value(value) {}
    virtual ~FValue();

    virtual const Value& at(String name) const;

    /**
     * @function set
     * @memberof FValue
     * @instance
     * @description Sets the calculation of a given field of this structure.
     * - Setting a value:
     * ```
     * // Defines calculations for a given field
     * class DynamicFields {
     * public:
     *   static wjson::Value sqrt_x(const wjson::Value& value) {
     *    return (int) sqrt(value.get("x", 0));
     *   }
     * };
     * // Sets sqrt_x as a dynamic calculated field
     * fvalue.set("sqrt_x", DynamicFields::sqrt_x);
     * // Gets the calculation result
     * double sqrt_x = fvalue.get("sqrt_x", 0);
     * ```
     * @param {string} name The field name
     * @param {function} getter A static method of type `wjson::Value (*getter)(const wjson::Value&)`.
     * @param {bool} [cache=true] If true, memorizes the last calculated value in the static `Value` data structure
     * @return {Value} This value allowing to chain set methods.
     */
    Value& set(String name, Value (*getter)(const Value&), bool cache = true);
  };
}

#endif
