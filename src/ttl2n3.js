#!/usr/bin/env node

/**
 * @function ttl2n3
 * @static
 * @description Converts a file from turtle to n-triples format
 *
 * - Only available as a command line application:
 * ```
 *     Usage: ./src/ttl2n3.js < input > output
 * ```
 */

// This command is equivalent to `rapper -i turtle filename -o ntriples > filename.n3`

const rdf = require('rdf');
const converter = input => rdf.TurtleParser.parse(input).graph.toArray().join("\n");

// Standard command to convert a file from a format to another
console.log(converter(require("fs").readFileSync(require("process").stdin.fd, "utf-8")));
